﻿using Microsoft.AspNetCore.Mvc;
using product.services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace product.services.Controllers
{
    
        [ApiController]
        [Route("api/[controller]")]
        public class ProductsController : ControllerBase
        {
            private static List<Product> _products = new List<Product>();

            [HttpGet]
            public ActionResult<IEnumerable<Product>> GetProducts()
            {
                return Ok(_products);
            }

            [HttpGet("{id}")]
            public ActionResult<Product> GetProduct(Guid id)
            {
                var product = _products.FirstOrDefault(p => p.Id == id);
                if (product == null)
                {
                    return NotFound();
                }

                return Ok(product);
            }

            [HttpPost]
            public ActionResult<Product> AddProduct(Product product)
            {
                if (product == null)
                {
                    return BadRequest("Product data is null.");
                }

                // Basic validation
                if (string.IsNullOrEmpty(product.Name) || string.IsNullOrEmpty(product.Description))
                {
                    return BadRequest("Product name or description cannot be empty.");
                }
                if (product.Price <= 0)
                {
                    return BadRequest("Price must be a positive value greater than zero.");
                }

            product.Id = Guid.NewGuid();
                _products.Add(product);
                return CreatedAtAction(nameof(GetProduct), new { id = product.Id }, product);
            }

            [HttpPut("{id}")]
            public IActionResult UpdateProduct(Guid id, Product product)
            {
                if (string.IsNullOrEmpty(product.Name) || string.IsNullOrEmpty(product.Description))
                {
                    return BadRequest("Product name or description cannot be empty.");
                }
                if (product.Price <= 0)
                {
                    return BadRequest("Price must be a positive value greater than zero.");
                }
                var existingProduct = _products.FirstOrDefault(p => p.Id == id);
                if (existingProduct == null)
                {
                    return NotFound();
                }


                existingProduct.Name = product.Name;
                existingProduct.Description = product.Description;
                existingProduct.Price = product.Price;

                return Ok();
            }

            [HttpDelete("{id}")]
            public IActionResult DeleteProduct(Guid id)
            {
                var productToRemove = _products.FirstOrDefault(p => p.Id == id);
                if (productToRemove == null)
                {
                    return NotFound();
                }

                _products.Remove(productToRemove);
                return Ok();
            }
        }
   


}
